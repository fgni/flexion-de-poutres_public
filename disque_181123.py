from math import pi
from math import copysign

#  Disque ou Anneau
class Disque: # d1 = Disque(12)
    def  __init__(self, diametre):
        # Diamètre (mm) du disque : obj.d
        self.d = diametre
    def __repr__(self): # Affichage de obj.d
        return f"Disque :\nDiamètre = {self.d}"
    def __str__(self): # print(obj.d)
        return f"{self.d}"

    def surface(self): # Section pleine : obj.surface()
        return copysign(pi, self.d) * self.d**2 /4

class Composition1: # c1 = Composition1([d1],[d2])
    def __init__(self, liste_pleins, liste_creux):
        self.listepleins = liste_pleins
        self.listecreux = liste_creux
    def surface(self):
        # Composition1([Disque(10)],[Disque(5),Disque(5)]).surface() = 39.27
        surface = 0
        for objet in self.listepleins :
            surface+= objet.surface()
        for objet in self.listecreux :
            surface-= objet.surface()
        return surface

class Composition2: # c1 = Composition2([d1,d2])
    def __init__(self, liste_disques):
        self.listedisques = liste_disques
    def surface(self):
        # Composition2([Disque(10),Disque(-5),Disque(-5)]).surface() = 39.27
        surface = 0
        for objet in self.listedisques :
            surface+= objet.surface()
        return surface

class Composition3: # c1 = Composition3([d1,d2])
    def __init__(self, liste_disques):
        self.listedisques = liste_disques
    def surface(self):
        # Composition3([Disque(10),Disque(-5),Disque(-5)]).surface() = 39.27
        # Somme des éléments d'une compréhension de liste
        return sum([objet.surface() for objet in self.listedisques])
    def volume(self):
        return sum([objet.volume() for objet in self.listedisques])

